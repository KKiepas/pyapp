from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login
from course.models import Course
# Create your views here.
def frontpage(request):
    courses = Course.objects.all()[0:10]
    return render(request, "frontpage.html", {'courses':courses})


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            account_type = request.POST.get('account_type', 'student')
            if account_type == 'instructor':
                user.userprofile.is_instructor = True
                user.userprofile.save()
            login(request, user)
            return redirect('frontpage')
    else:
        form = UserCreationForm()
    return render(request, "signup.html", {'form': form})
