from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Course(models.Model):
    name = models.CharField(max_length=255)
    instructor = models.ForeignKey(User, related_name='courses', on_delete=models.CASCADE)
    yt_channel = models.CharField(max_length=255)
    description = models.TextField()

    students = models.ManyToManyField(User, related_name="students", blank=True)
    # created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name