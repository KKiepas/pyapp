from django.shortcuts import render, redirect
from .models import Course
# Create your views here.


def course_detail(request, course_id):
    course = Course.objects.get(pk=course_id)
    return render(request, 'course_detail.html', {'course':course})


def assign_to_course(request, course_id):
    course = Course.objects.get(pk=course_id)
    course.students.add(request.user)
    course.save()
    return redirect('frontpage')


def unsign_to_course(request, course_id):
    course = Course.objects.get(pk=course_id)
    course.students.remove(request.user)
    course.save()
    return redirect('dashboard')
