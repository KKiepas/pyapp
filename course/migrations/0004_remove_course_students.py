# Generated by Django 3.1.5 on 2021-01-17 07:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0003_course_students'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='course',
            name='students',
        ),
    ]
