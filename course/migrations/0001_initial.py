# Generated by Django 3.1.5 on 2021-01-17 03:19

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('instructor', models.CharField(max_length=50)),
                ('yt_channel', models.CharField(max_length=255)),
                ('description', models.TextField()),
            ],
        ),
    ]
