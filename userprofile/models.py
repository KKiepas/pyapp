from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='userprofile', on_delete=models.CASCADE)
    is_instructor = models.BooleanField(default=False)
    # created_at = models.DateTimeField(auto_now_add=True)

User.userprofile = property(lambda  u:UserProfile.objects.get_or_create(user=u)[0])