from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from course.models import Course
from course.forms import AddCourseForm


# Create your views here.
@login_required
def dashboard(request):
    courses = request.user.courses.all()
    assign = request.user.students.all()
    return render(request, 'dashboard.html', {'userprofile': request.user.userprofile, 'courses': courses,
                                              'assign': assign})


@login_required()
def delete_course(request, course_id):
    course = Course.objects.get(pk=course_id)
    course.delete()
    return redirect('dashboard')


@login_required()
def add_course(request):
    if request.method == 'POST':
        form = AddCourseForm(request.POST)

        if form.is_valid():
            course = form.save(commit=False)
            course.instructor = request.user
            course.save()

            return redirect('dashboard')
    else:
        form = AddCourseForm()

    return render(request, 'add_course.html', {'form': form})
