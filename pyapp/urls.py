"""pyapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from core.views import frontpage, signup
from django.contrib.auth import views
from course.views import course_detail, assign_to_course, unsign_to_course
from userprofile.views import dashboard, delete_course, add_course

urlpatterns = [
    path('', frontpage, name='frontpage'),
    path('signup/', signup, name='signup'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('login/', views.LoginView.as_view(template_name='login.html'), name='login'),
    path('admin/', admin.site.urls),
    path('courses/<int:course_id>/', course_detail, name='course_detail'),
    path('dashboard/', dashboard, name='dashboard'),
    path('dashboard/<int:course_id>/', delete_course, name='course_delete'),
    path('dashboard/add', add_course, name='add_course'),
    path('assign/<int:course_id>/', assign_to_course, name='assign'),
    path('unsign/<int:course_id>/', unsign_to_course, name="unsign")
]
